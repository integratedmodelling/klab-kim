package org.integratedmodelling.kim.ui.elink;

import org.eclipse.xtext.ide.editor.syntaxcoloring.DefaultSemanticHighlightingCalculator;
import org.eclipse.xtext.ide.editor.syntaxcoloring.IHighlightedPositionAcceptor;
import org.eclipse.xtext.nodemodel.ICompositeNode;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.ui.editor.syntaxcoloring.DefaultHighlightingConfiguration;
import org.eclipse.xtext.util.CancelIndicator;
import org.integratedmodelling.kim.services.KimGrammarAccess;

import com.google.inject.Inject;

public class KimHighlightingCalculator extends DefaultSemanticHighlightingCalculator {

    @Inject
    KimGrammarAccess ga;

    @Override
    public void provideHighlightingFor(XtextResource resource, IHighlightedPositionAcceptor acceptor, CancelIndicator cancelIndicator) {
        ICompositeNode rootNode = resource.getParseResult().getRootNode();

        for (INode node : rootNode.getAsTreeIterable()) {
            
            if (false /* node.getGrammarElement() == ga.getAnnotationRule().getName() */) {
                acceptor.addPosition(node.getOffset(), node
                        .getLength(), DefaultHighlightingConfiguration.KEYWORD_ID);
            }
        }
        super.provideHighlightingFor(resource, acceptor, cancelIndicator);
    }

}
