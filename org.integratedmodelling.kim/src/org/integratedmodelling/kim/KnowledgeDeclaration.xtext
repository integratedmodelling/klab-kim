grammar org.integratedmodelling.kim.KnowledgeDeclaration

generate kdecl "http://www.integratedmodelling.org/kim/KnowledgeDeclaration"

import "http://www.eclipse.org/emf/2002/Ecore" as ecore

ConceptUnit:
	name=CAPITALIZED_ID |
	name=NAMESPACED_ID |
	presence?='presence' 'of' name=(CAPITALIZED_ID|NAMESPACED_ID) |
	count?='count' 'of' name=(CAPITALIZED_ID|NAMESPACED_ID) |
	distance?='distance' ('to'|'from') name=(CAPITALIZED_ID|NAMESPACED_ID) |
	probability?='probability' 'of' name=(CAPITALIZED_ID|NAMESPACED_ID) |
	uncertainty='uncertainty' 'of' name=(CAPITALIZED_ID|NAMESPACED_ID) |
	proportion?='proportion' 'of' name=(CAPITALIZED_ID|NAMESPACED_ID) ('in' other=(CAPITALIZED_ID|NAMESPACED_ID))? |
	ratio?='ratio' 'of' name=(CAPITALIZED_ID|NAMESPACED_ID) 'to' other=(CAPITALIZED_ID|NAMESPACED_ID) |
	value?='value' 'of' name=(CAPITALIZED_ID|NAMESPACED_ID) ('over' other=(CAPITALIZED_ID|NAMESPACED_ID))? |
	occurrence?='occurrence' 'of' name=(CAPITALIZED_ID|NAMESPACED_ID)
;

ConceptReference:
	name=ConceptUnit 
	(
		('of' inherent=ConceptElement) &
		('within' context=ConceptElement) &
		('identified' 'as' (stringIdentifier=(ID|STRING) | intIdentifier=INT) 'by' authority+=UPPERCASE_ID) &
		('by' classifier=(CAPITALIZED_ID|NAMESPACED_ID)) &
		('down' 'to' downTo=(CAPITALIZED_ID|NAMESPACED_ID))
	)?
;

ConceptDeclaration:
	others+=ConceptReference* name=ConceptReference
;

ConceptElement:
	reference=ConceptReference |
	'(' declaration=ConceptDeclaration ')'
;


ObservableSemantics:
	/*
	 * TODO
	 * in Unit 
	 * in Currency
	 * per spaceUnit
	 * into Classification
	 * according to property
	 * 
	 */
	name=LOWERCASE_PATH
;


ObservableDeclaration:
	// one or more ref or declaration 
	// optionally followed by the observation semantics modifiers
	elements+=ConceptElement+ semantics=ObservableSemantics?
;

terminal LOWERCASE_PATH:
	'^'?('a'..'z'|'_')('a'..'z'|'0'..'9'|'.')*
;

terminal CAPITALIZED_ID:
	'^'?('A'..'Z')('a'..'z'|'A'..'Z'|'0'..'9')*
;

terminal UPPERCASE_ID:
	'^'?('A'..'Z')('A'..'Z')*
;

terminal NAMESPACED_ID:
	LOWERCASE_PATH ':' CAPITALIZED_ID
;

terminal ID  		: '^'?('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*;
terminal INT returns ecore::EInt: ('0'..'9')+;
terminal STRING	: 
			'"' ( '\\' . /* 'b'|'t'|'n'|'f'|'r'|'u'|'"'|"'"|'\\' */ | !('\\'|'"') )* '"' |
			"'" ( '\\' . /* 'b'|'t'|'n'|'f'|'r'|'u'|'"'|"'"|'\\' */ | !('\\'|"'") )* "'"
		; 
terminal ML_COMMENT	: '/*' -> '*/';
terminal SL_COMMENT 	: '//' !('\n'|'\r')* ('\r'? '\n')?;


